
/**
 * Fichero: Break01.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class Break01 {

    public static void main(String args[]) {

        int contador = 0;
        while (contador < 100) {
            contador++;
            if (contador > 4) {
                break;
            }
            System.out.print(contador + " ");

        } // while
    } // main
}

/* EJECUCION:
 1 2 3 4 5
 */
