
/**
 *
 * Fichero: Par.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 19-oct-2015
 */
public class Par {

    public static void main(String argv[]) {

        int numero = 90;
        if (numero % 2 == 0) {
            System.out.println(numero + " es par");
        } else {
            System.out.println(numero + " es impar");
        }

    }

}
