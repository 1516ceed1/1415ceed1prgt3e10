
import java.util.Scanner;

/**
 *
 * Fichero: MenuDoWhile.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 2-nov-2015
 */
public class MenuDoWhile {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int opcion = 1;
        int suma = 0;
        System.out.println("Menu");
        System.out.println("0. Salir");
        System.out.println("1. Sumar 1");
        System.out.println("2. Restar 1");

        do {
            System.out.print("Opcion: ");
            opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    suma = suma + 1;
                    break;
                case 2:
                    suma = suma - 1;
                    break;
                case 0:
                    System.out.println("Fin");
                    break;
                default:
                    System.out.println("Introducir bien el numero");
                    break;
            }
            System.err.println("El valor de la suma es: " + suma);
        } while (opcion != 0);

    }

}
