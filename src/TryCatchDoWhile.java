
import java.util.Scanner;

/**
 *
 * Fichero: TryCatchDoWhile.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 09-nov-2015
 */
public class TryCatchDoWhile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        String linea;
        int numero;
        Boolean error = true;

        do {
            try {
                System.out.print("Numero: ");
                linea = sc.nextLine();
                numero = Integer.parseInt(linea);
                //numero = sc.nextInt();
                error = false;
            } catch (Exception e) {
                System.out.println("Error: No es numero.");
                //linea = sc.nextLine();
            }

        } while (error);

    }

}
