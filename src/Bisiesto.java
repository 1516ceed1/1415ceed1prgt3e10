
/**
 *
 * Fichero: Par.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 19-oct-2015
 */
public class Bisiesto {

   public static void main(String argv[]) {

      int numero = 2016;
      if ((numero % 4 == 0) && ((numero % 100 != 0) || (numero % 400 == 0))) {
         System.out.println("Es bisiesto el " + numero);
      } else {
         System.out.println("NO Es bisiesto el " + numero);
      }

   }

}
