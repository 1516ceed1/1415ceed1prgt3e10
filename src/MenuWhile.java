
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * Fichero: Menu.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-oct-2015
 */
public class MenuWhile {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int opcion = 1;
        int suma = 0;
        while (opcion != 0) {

            System.out.println("Menu");
            System.out.println("0. Salir");
            System.out.println("1. Sumar 1");
            System.out.println("2. Restar 1");
            System.out.print("Opcion: ");

            opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    suma = suma + 1;
                    break;
                case 2:
                    suma = suma - 1;
                    break;
                case 0:
                    System.out.println("Fin");
                    break;
                default:
                    System.out.println("Introducir bien el numero");
                    break;
            }
            System.out.println("El valor de la suma es: " + suma);

        }

    }

}
