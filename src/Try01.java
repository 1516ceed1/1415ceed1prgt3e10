
/**
 * Fichero: Try01.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class Try01 {

    public static void main(String[] args) {
        int num1 = 1, num2 = 0;
        System.out.println("El cociente es: " + (num1 / num2));
        System.out.println("Fin");
    }
}

/* EJECUCION:
 Exception in thread "main" java.lang.ArithmeticException: / by zero
 at try01.main(try01.java:7)
 */
