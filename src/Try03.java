
import java.io.IOException;

/**
 * Fichero: Try03.java Ejemplo de uso de throws Exception
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class Try03 {

    static void operacion() throws IOException {
        int num1 = 2, num2 = 0;
        System.out.println("El cociente es: " + (num1 / num2));

    }

    public static void main(String[] args) {
        try {
            operacion();
        } catch (ArithmeticException ae) {
            System.err.println("Error Aritmetico: " + ae.getMessage());
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        } finally {
            System.err.println("Fin");
        }
    }
}

/* EJECUCION:
 Error Aritmetico: / by zero
 Fin
 */
