/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * Fichero: Ifswith.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-oct-2015
 */
public class Ifswith {

    public static void main(String argv[]) {
        int n = 1;

        switch (n) {
            case 1:
                System.out.println("Tocado");
                break;
            case 2:
                System.out.println("Hundido");
                break;
            default:
                System.out.println("Sigue");
                break;
        }

        if (n == 1) {
            System.out.println("Tocado");
        } else if (n == 2) {
            System.out.println("Hundido");
        } else {
            System.out.println("Sigue");
        }

    }

}
