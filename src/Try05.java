
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Fichero: Try01.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class Try05 {

    public static void main(String[] args) {
        int numero = 0;
        try {

            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);
            System.out.println("Numero: ");
            String linea = br.readLine();
            numero = Integer.parseInt(linea);
            System.out.println("El numero leido es numero: " + numero);
        } catch (IOException | NumberFormatException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}

/* EJECUCION:
 Exception in thread "main" java.lang.ArithmeticException: / by zero
 at try01.main(try01.java:7)
 */
