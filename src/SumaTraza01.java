/**
 * Fichero: SumaTraza01.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class SumaTraza01 {

  public static void main(String[] args) {
    int s = 0, i = 1;
    System.out.println("TRAZA\n----");
    System.out.println(s + " " + i);
    while (i <= 3) {
      s = s + i;
      i++;
      System.out.println(s + " " + i);
    }
    System.out.println("La suma es: " + s);
  }
}
/* Ejecucion
TRAZA
----
0 1
1 2
3 3
6 4
La suma es: 6
*/
