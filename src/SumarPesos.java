
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * Fichero: SumarPesos.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 02-nov-2015
 */
public class SumarPesos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int suma = 0;
        int peso = 0;
        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < 5; i++) {
            System.out.print("Peso " + (i + 1) + ": ");
            peso = sc.nextInt();
            suma += peso;
        }
        System.out.println("La suma es " + suma);
    }

}
